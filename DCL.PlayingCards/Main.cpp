// Playing Cards
// Devon Lozier

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum class Rank
{
	TWO = 2,
	THREE = 3,
	FOUR =4,
	FIVE =5,
	SIX =6,
	SEVEN =7,
	EIGHT =8,
	NINE =9,
	TEN =10,
	JACK =11,
	QUEEN =12,
	KING = 13,
	ACE = 14,
	//JOKER should perhaps be included
};

enum class Suit
{
	SPADES,
	HEARTS,
	DIAMONDS,
	CLUBS
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card &card)
{
	switch (card.rank)
	{
	case Rank::TWO: cout << "The 2 "; break;
	case Rank::THREE: cout << "The 3 "; break;
	case Rank::FOUR: cout << "The 4 "; break;
	case Rank::FIVE: cout << "The 5"; break;
	case Rank::SIX: cout << "The 6 "; break;
	case Rank::SEVEN: cout << "The 7 "; break;
	case Rank::EIGHT: cout << "The 8 "; break;
	case Rank::NINE: cout << "The 9 "; break;
	case Rank::TEN: cout << "The 10 "; break;
	case Rank::JACK: cout << "The Jack "; break;
	case Rank::QUEEN: cout << "The Queen "; break;
	case Rank::KING: cout << "The King "; break;
	case Rank::ACE: cout << "The Ace "; break;
	}

	switch (card.suit)
	{
	case Suit::SPADES: cout << "of Spades\n"; break;
	case Suit::HEARTS: cout << "of Hearts\n"; break;
	case Suit::DIAMONDS: cout << "of Diamonds\n"; break;
	case Suit::CLUBS: cout << "of Clubs\n"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	int rank1;
	int rank2;
	switch (card1.rank)
	{
	case Rank::TWO: rank1 = 2;
		break;
	case Rank::THREE: rank1 = 3;
		break;
	case Rank::FOUR: rank1 = 4;
		break;
	case Rank::FIVE: rank1 = 5;
		break;
	case Rank::SIX: rank1 = 6;
		break;
	case Rank::SEVEN: rank1 = 7;
		break;
	case Rank::EIGHT: rank1 = 8;
		break;
	case Rank::NINE: rank1 = 9;
		break;
	case Rank::TEN: rank1 = 10;
		break;
	case Rank::JACK: rank1 = 11;
		break;
	case Rank::QUEEN: rank1 = 12;
		break;
	case Rank::KING: rank1 = 13;
		break;
	case Rank::ACE: rank1 = 14;
		break;
	}
	switch (card1.suit)
	{
	case Suit::HEARTS:
		break;
	case Suit::DIAMONDS:
		break;
	case Suit::CLUBS:
		break;
	case Suit::SPADES:
		break;
	}

	switch (card2.rank)
	{
	case Rank::TWO: rank2 = 2;
		break;
	case Rank::THREE: rank2 = 3;
		break;
	case Rank::FOUR: rank2 = 4;
		break;
	case Rank::FIVE: rank2 = 5;
		break;
	case Rank::SIX: rank2 = 6;
		break;
	case Rank::SEVEN: rank2 = 7;
		break;
	case Rank::EIGHT: rank2 = 8;
		break;
	case Rank::NINE: rank2 = 9;
		break;
	case Rank::TEN: rank2 = 10;
		break;
	case Rank::JACK: rank2 = 11;
		break;
	case Rank::QUEEN: rank2 = 12;
		break;
	case Rank::KING: rank2 = 13;
		break;
	case Rank::ACE: rank2 = 14;
		break;
	}
	switch (card2.suit)
	{
	case Suit::HEARTS:
		break;
	case Suit::DIAMONDS:
		break;
	case Suit::CLUBS:
		break;
	case Suit::SPADES:
		break;
	}

	return card1, card2;
}

//Card HighCard(Card card1, Card card2);

int main()
{
	Card c1;
	Card c2;
	c1.rank = Rank::QUEEN;
	c1.suit = Suit::HEARTS;
	c2.rank = Rank::KING;
	c2.suit = Suit::CLUBS;

	PrintCard(c1);
	PrintCard(c2);

	Card card1 = c1;
	Card card2 = c2;
	HighCard(card1, card2);

	if (card1.rank < card2.rank)
	{
		cout << "The second card has the higher rank.";
	}
	else if (card2.rank < card1.rank)
	{
		cout << "The first card has the higher rank.";
	}
	else if (card1.rank == card2.rank)
	{
		cout << "These cards' ranks are equal.";
	}	

	(void)_getch();
	return 0;
}